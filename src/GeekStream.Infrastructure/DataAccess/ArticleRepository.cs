﻿using System;
using System.Collections.Generic;
using System.Linq;
using GeekStream.Domain.Entities;
using GeekStream.Domain.Interfaces.DataAccess;

namespace GeekStream.Infrastructure.DataAccess
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly ApplicationDbContext _context;
        public ArticleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddArticle(Article article)
        {
            article.PublishedDate = DateTime.MinValue;
            _context.Add(article);
            _context.SaveChanges();
        }

        public void DeleteArticle(int id)
        {
            _context.Remove(GetArticleById(id));
            _context.SaveChanges();
        }

        public Article GetArticleById(int id)
        {
            return _context.Articles.Single(a => a.Id == id);
        }

        public IEnumerable<Article> GetArticles()
        {
            return _context.Articles.ToList();
        }

        public void PublishArticle(int id)
        {
            var article = GetArticleById(id);
            article.PublishedDate = DateTime.Now;
            _context.SaveChanges();
        }

        public void UnpublishArticle(int id)
        {
            var article = GetArticleById(id);
            article.PublishedDate = DateTime.MinValue;
            _context.SaveChanges();
        }
    }
}
