﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;
using GeekStream.Domain.Interfaces.DataAccess;

namespace GeekStream.Domain.Services
{
    class ArticlesService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticlesService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<Article> GetArticles()
        {
            return _articleRepository.GetArticles();
        }
    }
}
