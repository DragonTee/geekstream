﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;

namespace GeekStream.Domain.Interfaces.DataAccess
{
    public interface IArticleRepository
    {
        IEnumerable<Article> GetArticles();
        Article GetArticleById(int id);
        void AddArticle(Article article);
        void PublishArticle(int id);
        void UnpublishArticle(int id);
        void DeleteArticle(int id);
    }
}
