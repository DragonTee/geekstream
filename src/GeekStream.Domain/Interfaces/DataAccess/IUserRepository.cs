﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;

namespace GeekStream.Domain.Interfaces.DataAccess
{
    interface IUserRepository
    {
        IEnumerable<User> GetUsers();
    }
}
