﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Domain.Entities
{
    public class User
    {
        public User()
        {

        }

        public User(int id, string firstName, string lastName, string hashedPassword)
        {
            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new ArgumentException(nameof(firstName));
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ArgumentException(nameof(lastName));
            }

            if (string.IsNullOrWhiteSpace(hashedPassword))
            {
                throw new ArgumentException(nameof(hashedPassword));
            }

            Id = id;
            FirstName = firstName;
            LastName = lastName;
            HashedPassword = hashedPassword;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
        public string HashedPassword { get; set; }
    }
}
