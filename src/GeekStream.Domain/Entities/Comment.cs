﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Domain.Entities
{
    public class Comment
    {
        public Comment()
        {

        }

        public Comment(int id, string content, User author)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                throw new ArgumentException(nameof(content));
            }

            Content = content;
            Author = author;
            Id = id;
        }

        public int Id { get; set; }
        public string Content { get; set; }
        public User Author { get; set; }
    }
}
