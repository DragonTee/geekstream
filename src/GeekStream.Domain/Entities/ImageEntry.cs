﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Domain.Entities
{
    public class ImageEntry
    {
        public ImageEntry(int id, string imagePath)
        {
            Id = id;
            ImagePath = imagePath;
        }

        public int Id { get; set; }
        public string ImagePath { get; set; }
    }
}
