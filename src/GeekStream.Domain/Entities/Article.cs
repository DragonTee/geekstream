﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeekStream.Domain.Entities
{
    public class Article
    {
        public Article()
        {

        }

        public Article(int id, User author, string title, string content, Category category, string tags, int rating, ICollection<ImageEntry> images, ICollection<Comment> comments, ICollection<User> reviewers, DateTime publishedDate)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                throw new ArgumentException(nameof(title));
            }

            if (string.IsNullOrEmpty(content))
            {
                throw new ArgumentException(nameof(content));
            }

            if (category == null)
            {
                throw new ArgumentException(nameof(category));
            }

            if (tags == null)
            {
                throw new ArgumentException(nameof(tags));
            }

            Title = title;
            Content = content;
            Category = category;
            Tags = tags;
            Rating = rating;
            Id = id;
            Author = author;
            Images = images;
            Comments = comments;
            Reviewers = reviewers;
            PublishedDate = publishedDate;
        }

        public string Content { get; set; }
        public string Title { get; set; }
        public Category Category { get; set; }
        public string Tags { get; set; }
        public int Rating { get; set; }
        public int Id { get; set; }
        public User Author { get; set; }
        public ICollection<ImageEntry> Images { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<User> Reviewers { get; set; }
        public DateTime PublishedDate { get; set; }
    }
}
