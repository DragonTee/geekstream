﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;
using Xunit;

namespace GeekStream.UnitTests.DomainTests
{
    public class CommentTests
    {
        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullContent()
        {
            int id = 0;
            string content = null;
            var author = new User();

            Assert.Throws<ArgumentException>(() =>
            {
                var comment = new Comment(id, content, author);
            });
        }
    }
}
