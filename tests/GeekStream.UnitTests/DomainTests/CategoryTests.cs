﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;
using Xunit;

namespace GeekStream.UnitTests.DomainTests
{
    public class CategoryTests
    {
        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullName()
        {
            string name = null;
            int id = 0;

            Assert.Throws<ArgumentException>(() =>
            {
                var category = new Category(name, id);
            });
        }
    }
}
