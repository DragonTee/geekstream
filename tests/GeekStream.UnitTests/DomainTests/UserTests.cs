﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;
using Xunit;

namespace GeekStream.UnitTests.DomainTests
{
    public class UserTests
    {
        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullFirstName()
        {
            int id = 0;
            string firstName = null;
            string lastName = "TestName";
            string hashedPass = "Abc";

            Assert.Throws<ArgumentException>(() =>
            {
                var user = new User(id, firstName, lastName, hashedPass);
            });
        }

        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullLastName()
        {
            int id = 0;
            string firstName = "TestName";
            string lastName = null;
            string hashedPass = "Abc";

            Assert.Throws<ArgumentException>(() =>
            {
                var user = new User(id, firstName, lastName, hashedPass);
            });
        }

        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullHashedPassword()
        {
            int id = 0;
            string firstName = "TestName";
            string lastName = "TestName";
            string hashedPass = null;

            Assert.Throws<ArgumentException>(() =>
            {
                var user = new User(id, firstName, lastName, hashedPass);
            });
        }
    }
}
