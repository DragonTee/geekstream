﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeekStream.Domain.Entities;
using Xunit;

namespace GeekStream.UnitTests.DomainTests
{
    public class ArticleTests
    {
        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullTitle()
        {
            int id = 0;
            string title = null;
            string content = "Abacaba";
            var category = new Category("Abc", 0);
            string tags = "Abacaba";
            int rating = 0;
            var author = new User();
            var images = new List<ImageEntry>();
            var comments = new List<Comment>();
            var reviewers = new List<User>();
            var publishedDate = DateTime.MinValue;

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, author, title, content, category, tags, rating, images, comments, reviewers, publishedDate);
            });
        }

        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullContent()
        {
            int id = 0;
            string title = "Abacaba";
            string content = null;
            var category = new Category("Abc", 0);
            string tags = "Abacaba";
            int rating = 0;
            var author = new User();
            var images = new List<ImageEntry>();
            var comments = new List<Comment>();
            var reviewers = new List<User>();
            var publishedDate = DateTime.MinValue;

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, author, title, content, category, tags, rating, images, comments, reviewers, publishedDate);
            });
        }

        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullCategory()
        {
            int id = 0;
            string title = "Abacaba";
            string content = "Abacaba";
            Category category = null;
            string tags = "Abacaba";
            int rating = 0;
            var author = new User();
            var images = new List<ImageEntry>();
            var comments = new List<Comment>();
            var reviewers = new List<User>();
            var publishedDate = DateTime.MinValue;

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, author, title, content, category, tags, rating, images, comments, reviewers, publishedDate);
            });
        }

        [Fact]
        public void Constructor_ShouldFail_WhenWePassNullTags()
        {
            int id = 0;
            string title = "Abacaba";
            string content = "Abacaba";
            var category = new Category("Abc", 0);
            string tags = null;
            int rating = 0;
            var author = new User();
            var images = new List<ImageEntry>();
            var comments = new List<Comment>();
            var reviewers = new List<User>();
            var publishedDate = DateTime.MinValue;

            Assert.Throws<ArgumentException>(() =>
            {
                var article = new Article(id, author, title, content, category, tags, rating, images, comments, reviewers, publishedDate);
            });
        }
    }
}
